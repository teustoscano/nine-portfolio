import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/onze.jpeg'

const Onze = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="5:1 Cálculo Vetorial" 
                height={500} 
                word="Teste"
                opinion="Foi estranho responder o questionário online no formato de prova com a dupla, pois estamos acostumados a responder no moodle individualmente. E o fato de ter que responder no papel, tirar foto e anexar é trabalhoso."
                positive="Ter a dupla como incetivo e ajuda. "
                negative="Ter que fazer à mão primeiro."
                about="Foi um resumo, e assim como as últimas atividades tem bastante aplicação."
            />
        </Layout>
    )
}

export default Onze