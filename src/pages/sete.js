import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/arte.png'

const Sete = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="3:1 Integrais duplas em retângulos. Teoria de Fubini. Integrais duplas em regiões reais" 
                height={420} 
                word="Artístico"
                opinion="Para nossa primeira obra de arte escolhemos fazer uma pintura em papel de aquarela de 300g/m. Demonstrando as áreas que foram estudadas"
                positive="Expressar a criatividade, poder mostrar um talento da dupla."
                negative="Nada a declarar"
                about="É importante para encontrar o volume gráfico das regiões retangulares do plano xy."
            />
        </Layout>
    )
}

export default Sete