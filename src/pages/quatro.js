import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/jornal.png'

const Quatro = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="2:1 Funções de Várias Variavéis Reais. Limites e Continuidade" 
                height={500} 
                word="Dificuldade"
                opinion="Fazer um jornal informativo sobre a matéria. "
                positive="É uma atividade com resultado bem explicativo. "
                negative="Fazer um jornal não é fácil, requer muito tempo, paciência e pesquisa."
                about="Por ter limites facilitou a forma de entender essa nova matéria."
            />
        </Layout>
    )
}

export default Quatro