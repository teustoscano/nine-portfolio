import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/um.png'

const Um = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="1:1 Vetores e a Geometria no espaço" 
                height={400} 
                word="Interessante"
                opinion="Já é uma prática comum entre os estudantes fazerem o mapa mental, uma vez que ajuda na fixação do conhecimento. "
                positive="Foi uma surpresa, por ser o primeiro trabalho prático de uma disciplina que nos, como dupla, esperávamos apenas pela teoria. "
                negative="Nada a declarar."
                about="Temos um pouco de dificuldade em sistemas tridimensionais, por não serem planos tão tangíveis."
            />
        </Layout>
    )
}

export default Um
