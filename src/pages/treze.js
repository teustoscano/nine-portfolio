import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/treze.png'

const Treze = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="5:3 Projeto Teórico em HQ" 
                height={500} 
                word="Alívio e Gratidão"
                opinion="Divertido passar um tópico de cálculo para uma mídia tão popular como o quadrinho. Fica visualmente mais chamativo e traz elementos que podem ajudar a entender a matéria."
                positive="Exercícios com boa fluidez e que demonstram bem a matéria. "
                negative="Matéria que exige mais tempo de estudo."
                about="Nosso último trabalho de Cálculo III, mais uma etapa concluída. Foi legal fazer uma HQ sobre as teorias, deixar os alunos soltos na imaginação é algo construtivo para nosso aprendizado.."
            />
        </Layout>
    )
}

export default Treze