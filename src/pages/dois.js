import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/dois.png'

const Dois = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="1:2 Equação da Reta e do Plano no Espaço" 
                height={500} 
                word="Trabalhoso"
                opinion="Foram duas questões onde uma era de verdadeiro ou falso e a segunda de cálculo. "
                positive="Quando escrevemos e aplicamos os cálculos a fixação na mente é melhor. "
                negative="Foi muito extenso e o áudio dificultou bastante pois é difícil expressar certas contas."
                about="Um conteúdo difícil de entender na parte gráfica, porém de fácil absorção na parte teórica."
            />
        </Layout>
    )
}

export default Dois