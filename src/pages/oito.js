import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/oito.png'

const Oito = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="3:2 Integrais Múltiplas" 
                height={500} 
                word="Oferta"
                opinion="Foi curioso saber que iriamos vender integrais duplas, afinal teriamos que convencer as pessoas a comprarem. "
                positive="Exercer a criatividade dos alunos, fazer flyers que fossem capazes de intrigar a vontade do cliente de adquirir o produto. Tudo isso levando o conteúdo de uma forma descontraída. "
                negative="A matéria é muito densa e foi cansativo."
                about="Por se tratar de um conteúdo denso, a absorcão foi mais complicada que os demais, são muitos pontos importantes que precisamos prestar atenção. Como: Coordenadas Polares, que é importante para a integração de discos, retangulos e funções de duas variáveis."
            />
        </Layout>
    )
}

export default Oito