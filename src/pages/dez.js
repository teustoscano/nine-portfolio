import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/nove2.png'

const Dez = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="4:2 Funções Vetoriais" 
                height={500} 
                word="Animação"
                opinion="A partir dessa unidade já começamos a procurar criatividade, ânimo, tempo para continuar a fazer os exercícios com maestria, de fato, nÃo tem sido simples."
                positive="Por ser mais parecido com outros exercícios não foi tão complicado achar os softwares para desenvolver. "
                negative="Repetitivo."
                about="É capaz de demonstrar a partícula se movendo em uma trajetória curvilínea, usado no cálculo vetorial. Podemos notar que uma curvatura da hélice circular é constante e diminui com o crescimento em valor absoluto de r ou a."
            />
        </Layout>
    )
}

export default Dez