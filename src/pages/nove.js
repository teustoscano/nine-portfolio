import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/dez.jpeg'

const Nove = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="3:4 Integrais Múltiplas " 
                height={550} 
                word="Explicativa"
                opinion="O contato com as demais duplas nos trouxe uma otima sensação, pois nos fez relembrar do contato na sala de aula. É importante a interação da turma no aprendizado de todos, pois o aluno faz perguntas que para o professor pode passar despercebido"
                positive="Contato com os colegas de classe. "
                negative="Nada a declarar."
                about="Há muita aplicação nos demais conteúdos estudados na matéria de cálculo, como exemplo o cálculo de volume da esfera."
            />
        </Layout>
    )
}

export default Nove