import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/tres.png'

const Tres = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="1:3 Cilíndros e Superfícies Quádricas" 
                height={400} 
                word="Desafiador"
                opinion="Para esse tema escolhemos fazer um Quizz Online, porque acreditamos que jogar é uma metodologia prática, rápida e divertida de aprender. "
                positive="Deixar o tema livre foi uma boa sacada, assim podemos soltar a imaginação e criatividade e fazer o que melhor achamos que irá fixar o conteúdo. "
                negative="Tema livre pode ser desafiador, porque existem N possibilidades do que fazer o que nos deixa perdido, as vezes."
                about="Usamos a equação do cone, o que nos fez aprender e conseguir visualizar as rotações nos eixos."
            />
        </Layout>
    )
}

export default Tres