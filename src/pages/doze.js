import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/doze.png'

const Doze = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="5:2 Fixando a Teoria" 
                height={540} 
                word="Divertido"
                opinion="Pintar as equações de acordo com a legenda e resolver as questões explicando por áudio. "
                positive="Interativo, relaxante, remete as atividades do fundamental. "
                negative="Questões com nível de dificuldade alta e ter a exigência do áudio dificulta para a dupla."
                about="Foi o mais difícil de entender até o momento, pois são teorias mais complexas."
            />
        </Layout>
    )
}

export default Doze