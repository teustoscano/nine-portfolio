import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/cinco.png'

const Cinco = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="2:2 Derivada Parcial. Plano Tangente e Aproximação Linear. Regra da Cadeia" 
                height={500} 
                word="Animação"
                opinion="Foi interessante fazer a produção do vídeo, conseguimos trazer alguns professores do qual ja tivemos aula (Prof Fragelli, Prof.a Luiza Yoko e Prof.a Tati), o que nos deu mais animo e criatividade para o sub-tema."
                positive="Ótima oportunidade que tivemos de aprender a usar softwares e entender melhor a regra da cadeia. "
                negative="Nada a declarar."
                about="Para conseguir produzir o vídeo tivemos que estudar sobre o tema Regra da cadeia, automaticamente algumas duvidas que tinhamaos foram esclarecidas como:As variáveis intermediarias e suas dependências."
            />
        </Layout>
    )
}

export default Cinco