import React from 'react'
import Layout from "../components/layout"
import Pages from '../components/Pages'
import foto from '../images/seis.jpeg'

const Seis = () => {
    return (
        <Layout>
            <Pages
                pic={foto}
                title="2:3 Derivadas Parciais" 
                height={700} 
                word="Frustração"
                opinion="O infografico foi didático, tinha explciações a cerca do conteúdo, exemplos, atividades, porem ao finalizar o site deu erro e não conseguimos exportar. "
                positive="A oportunidade de conhecer a vida do matemático frances Joseph Louis Lagrange, pois para embasarmos o conteúdo pesquisamos tambem sobre quem era a pessoa por trás dos calculos. "
                negative="A ferramenta que escolhemos para criar o infográfico foi ruim, nessa altura das atividades já estavamos cansadas pois as provas das outras matérias já haviam começado e foi díficl conciliar tudo."
                about="Escolhemos o tema Multiplicadores de Lagrange e foi interessante conhecer o método de definiçÃo das novas variavéis, e ter a demonstração no cilindro, uma geometria de fácil visualização."
            />
        </Layout>
    )
}

export default Seis