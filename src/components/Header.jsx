import React from 'react'
import './header.scss'
import { Link } from "gatsby"
import unb from '../images/unb.png'

const Header = () => {
    return (
        <section className="H-wrapper">
            <Link to="/">
                <img src={unb} />
                <h1>Portfólio - Grupo 60</h1>
                <small>Ingryd Karine & Ana Beatriz</small>
            </Link>

            <p>Aperte nas imagens para saber mais sobre</p>
        </section>
    )
}

export default Header
