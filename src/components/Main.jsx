import React from 'react'
import './main.scss'
import { Link } from 'gatsby'

const Main = () => {
    return (
        <section className="M-wrapper">
            <div className="M-top">
                <Link to="/um">
                    <div />
                </Link>
                <Link to="/dois">
                    <div />
                </Link>
                <Link to="/tres">
                    <div />
                </Link>
            </div>
            <div className="M-mid">
                <Link to="/quatro">
                    <div />
                </Link>
                <Link to="/cinco">
                    <div />
                </Link>
                <Link to="/seis">
                    <div />
                </Link>
            </div>
            <div className="M-bot">
                <Link to="/sete">
                    <div />
                </Link>
                <Link to="/oito">
                    <div />
                </Link>
                <Link to="/nove">
                    <div />
                </Link>
            </div>
            <div className="M-last">
                <Link to="/dez">
                    <div />
                </Link>
                <Link to="/onze">
                    <div />
                </Link>
                <Link to="/doze">
                    <div />
                </Link>
            </div>
            <div className="M-real-last">
                <Link to="/treze">
                    <div />
                </Link>
            </div>
        </section>
    )
}

export default Main
