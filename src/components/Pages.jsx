import React from 'react'
import './pages.scss'
import { Link } from 'gatsby'

const Pages = ({ pic, title, height, word, opinion, positive, negative, about }) => {
    return (
        <section className="P-wrapper">
            <div className="P-left">
                <img src={pic} height={height} />
            </div>
            <div className="P-right">
                <h4 className="P-title">{title}</h4>
                <p>{opinion}</p>
                <h4>Palavra Definição:</h4>
                <p>{word}</p>
                <h4>Pontos Positivos:</h4>
                <p>{positive}</p>
                <h4>Pontos Negativos:</h4>
                <p>{negative}</p>
                <h4>Sobre o Conteúdo:</h4>
                <p>{about}</p>
            </div>
            <Link to="/">
                voltar
                </Link>
        </section>
    )
}

export default Pages
